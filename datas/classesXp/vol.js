const listVols = new Array();
module.exports = class Vol {
  // Constructeur -------------------------------------------------------------
  constructor(voleur, victime, xp, tps){
    this.voleur = voleur;
    this.victime = victime;
    this.xp = xp;
    this.vu = false;
    this.tps = tps;
    listVols.push(this);
  }

  // Methodes -----------------------------------------------------------------
  static checkVols(user){
    var listVu = new Array();
    var listIndex = new Array()
    var index = 0;
    listVols.forEach(function(vol){
      if (vol != null){
        if (vol.victime == user && vol.tps >= Date.now()){
            listVu.push(vol);
            vol.vu = true;
        }
      }
      index++;
    });
    for (var i in listIndex) listVols[i].vu = true;
    return listVu;
  }
  static clearVols(user){
    var listIndex = new Array()
    var index = 0;
    listVols.forEach(function(vol){
      if (vol != null){
        if ((vol.victime == user || vol.voleur == user) && (vol.tps < Date.now() || vol.vu)){
          listIndex.push(index);
        }
      }
      index++;
    });
    for (var i in listIndex) listVols[i] = null;
  }

}
