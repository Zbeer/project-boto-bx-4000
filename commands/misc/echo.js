module.exports = {
	name: 'echo',
	alias: ['repeat', 'repete'],
	description: 'Répète une phrase loul',
	serverOnly: true,
	args: true,
	cooldown: 0.1,
	usage: '[Ce qu\'il doit répeter]',
	execute(message, args) {
		message.channel.send(args.join(' '))
			.then(() => message.delete());

	},
};
