// Fonctions de jets
const Discord = require('discord.js');

module.exports = {
	name: 'jets',
	description: 'Lance un dé XdY',
	alias: ['jet', 'dé', 'lance'],
	execute(msg, args) {
		if (args[0].charAt(1) == 'd') {
			jdrJets(msg, args);
		}else{
			msg.delete();
			msg.channel.send('Erreur jet invalide');}
	},
};

function jdrJets(msg, args) {
	let mesNb = new Array();
	mesNb = args[0].split('d');
	try{
		const embed = new Discord.RichEmbed()
			.setTitle('Lancé de ' + parseInt(mesNb[0], 10) + ' dé(s) :')
			.setColor(2671393)
			.setThumbnail('https://i.imgur.com/gsSKMuS.gif');

		if (isNaN(mesNb[0]) || isNaN(mesNb[1])) {
			throw 'error mdr';
		}
		if (parseInt(mesNb[0], 10) > 25) {
			msg.channel.send('Trop de jets! J\'ai la flemme bro...');
		}else if (parseInt(mesNb[1], 10) >= 100000) {
			msg.channel.send(mesNb[1] + '!? That\'s too much!');
		}else if (parseInt(mesNb[0], 10) < 0 || parseInt(mesNb[1], 10) < 0) {
			msg.channel.send('Pourquoi être si négatif?');
		}else if (parseInt(mesNb[0], 10) === 0 || parseInt(mesNb[1], 10) === 0) {
			msg.channel.send('c nul...');
		}else{
			for(let i = 1 ; i < parseInt(mesNb[0], 10) + 1; i++) {
				embed.addField('Jet n°' + i + ' :', +(Math.floor(Math.random() * Math.floor(mesNb[1])) + 1), true);
			}
			msg.channel.send({ embed });
		}
	}catch(error) {
		console.log(error);
		msg.channel.send('Couillon fait une bonne syntaxe! XdX X étant un nombre');
	}
}