const combat = require('./classes/classeCombat.js');
const fs = require('fs');
const status = require('../../utils/status.json');

let monCombat;

module.exports = {
	name: 'combat',
	description: 'Lance un combat selon les règles d\'Aoochu.',
	execute(msg, args) {
	// combat ou rdy ou done
		try {
			loadCombat(msg.client);
			monCombat = new combat(0);
			launchCombat(msg, msg.client);
		} catch (err) { console.log(err); }
	},
};

function loadCombat(client) {
	const commandFiles = fs.readdirSync('./commands/jdr/combat_ext/').filter(file => file.endsWith('.js'));
	for (const file of commandFiles) {
		const command = require(`./combat_ext/${file}`);
		//	si la commande n'est pas encore dans status.json, on les rajoutes
		if(!Object.keys(status).some(key => key === command.name)) {
			status[command.name] = true;
		}
		command.status = status[command.name];
		client.commands.set(command.name, command);
	}
}

// Fonctions de combats
function launchCombat(msg, client) {
	if (msg.author.id == '192933855110365184') {
		//  loadCombat(discord_client);
		msg.channel.send('Qui est prêt pour le combat? (b!rdy)');
		monCombat.setAppel(true);
		monCombat.stockerJSON();
		console.log(monCombat);
		setTimeout(function() {
			monCombat = combat.fromJSON();
			monCombat.setAppel(false);
			jdrCombat(msg);
			delete require.cache[require.resolve('./combat_ext/rdy.js')];
			client.commands.delete('rdy');

		}, 30000);
	} else {
		msg.channel.send('T\'es pas MJ couillon!');
	}
}

function jdrCombat(msg) {
	let gensPresent = '';
	monCombat.getCombattants().forEach(function(nom) {
		gensPresent += nom + '/';
	});
	if(gensPresent != 0) {

		msg.channel.send('Les combattants : ' + gensPresent.substring(0, gensPresent.length - 1));

		monCombat.initOrdre();
		monCombat.trierOrdre();
		monCombat.showOrdre(msg);
		monCombat.nouveauRound();
		msg.channel.send('__***DEBUT DU COMBAT***__');
		monCombat.setCombat(true, msg);
		monCombat.stockerJSON();
	} else { msg.channel.send('Pas de combattants.'); }
}
