const Discord = require('discord.js');
const Server = require('../../../datas/classesXp/server.js');

module.exports = {
	name: 'accept',
	description: 'Autoriser ou non l\'xp sur le serveur | Autoriser ou non le vol sur le serveur.',
	serverOnly: true,
  args: true,
  usage: '[oui|non]',
	execute(message, args, attrib) {
    // On regarde s'il a accepté ou non
		var accept = (args[0].toLowerCase() == 'oui') ? true : false;

    // On récupère le serveur (ou on le crée)
    var server = Server.getServer(message.guild.id);
    if (server == null) server = Server.baseServer(message.guild.id);

    // On modifie soit l'xp soit le vol en fonction de la commande
    var text;
    if (attrib == "xp") {
      server.setAcceptXp(accept);
      text = (accept) ? "accepte l'utilisation de l'xp !" : "n'accepte plus l'utilisation de l'xp !";
    }
    else {
      server.setAcceptThief(accept);
      text = (accept) ? "accepte le vol d'xp !" : "n'accepte plus le vol d'xp !";
    }

    // On display ce qui a été fait
    return message.channel.send('`✔ Ce serveur ' + text + '`');
	},
};
