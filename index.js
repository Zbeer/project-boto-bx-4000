const fs = require('fs');
const Discord = require('discord.js');
const { prefix, token } = require('./config.json');

const client = new Discord.Client();
client.commands = new Discord.Collection();
const rCheck = require('./utils/rightcheck.js');
const cooldowns = new Discord.Collection();

const replier = require('./utils/replier.js');
const status = require('./utils/status.json');
const User = require('./datas/classesXp/user.js');
const Server = require('./datas/classesXp/server.js');
const reps = JSON.parse(fs.readFileSync('./utils/reps.json'));
client.once('ready', () => {
	console.log('Ready!');
});

//	====== Chargement des fonctions
console.log('\n\n     ||==================================||');
console.log('     ||   Starting to load commands...   ||');
console.log('     ||==================================||\n');
const repertories = fs.readdirSync('./commands').filter(file => !file.includes('.'));
let oopsie = false;
let oopsies = "\nOopsies something went wrong : \n"
repertories.forEach(rep => {
	try{
		const commandFiles = fs.readdirSync(`./commands/${rep}`).filter(file => file.endsWith('.js'));
		console.log(`\n+----+ Loading repertory : ${rep}\t\t=> ${commandFiles.length} commands`);
		//	On charge les commandes
		let hasNotSucceed = new Array();
		for (const file of commandFiles) {
			try{
				console.log(`     |\n     |----- Loading command : ${file}...`);
				const command = require(`./commands/${rep}/${file}`);
				//	si la commande n'est pas encore dans status.json, on les rajoutes
				if(!Object.keys(status).some(key => key === command.name)) {
					status[command.name] = true;
				}
				command.repertory = rep;
				command.status = status[command.name];
				client.commands.set(command.name, command);
				console.log('     |     => Successfully loaded.');
			}catch(err) {
				console.error(err);
				console.log();
				hasNotSucceed.push(file);
			}
		}
		if (hasNotSucceed.length != 0){
			oopsie = true;
			oopsies += `==> rep : ${rep} < problème avec la/les commande(s) suivantes : [ `;
			hasNotSucceed.forEach(cmd => {
				oopsies += `${cmd} `;
			})
			oopsies += "] >\n";
		}
		let repExist = false;
		reps.repertories.forEach(repz => { if (repz.name == rep) { repExist = true;	}});
		if (!repExist) {
			reps.repertories.push({ 'name':`${rep}`, 'desc':`Toutes les fonctions de ${rep}` });
		}

	} catch(err) {
		console.error(err);
		console.log();
	}
});

fs.writeFileSync('./utils/status.json', JSON.stringify(status, null, 2));
fs.writeFileSync('./utils/reps.json', JSON.stringify(reps, null, 2));

if (oopsie){
	console.log(oopsies);
} else {
	console.log("\nEverything was reloaded without any problem!")
}

console.log('\n\n==========================================================================\n\n');
//	====== fin de chargement des fonctions

client.on('message', message => {

	// Xp -----------------------------------------------------------------------
	if (!message.author.bot && message.guild != null){
		var serv = Server.getServer(message.guild.id);
		if (serv == null) serv = Server.baseServer(message.guild.id);
		if (serv.acceptXp) {
			var user = User.getUser(message.author.id);
			if (user == null) user = User.baseUser(message.author.id, message.guild.id);
			var lvl = user.getXpLevel();
			var max = Math.floor((Server.nextLvl(user.xp) - (25 * lvl * lvl)) * 0.01);
			if (max - (lvl/4) <= 2) max = 2;
			else max -= lvl / 4;
			user.addXp(Math.floor(Math.random() * max + 1));
			if (user.getXpLevel() > lvl){
				message.channel.send("```md\n* " + message.author.username + " vient de passer niveau < " + user.getXpLevel() + " > !```");
			}
		}
	}
  // --------------------------------------------------------------------------

	//	check si le message contient une commande
	if (!message.content.startsWith(prefix)) {

		//	On check si un utilisateur a mentionné le bot (et n'a fait uniquement cela) et on lui répond en lui donnant le préfix du bot
		const mention = message.mentions.users;
		if (message.content.split(/ +/).length == 1 && mention.size == 1 && mention.first().id === client.user.id) {
			replier.getPrefix(message);
		}
		//	on check si l'utilisateur déclenche l'un des nombreux replier (yen a qu'un pour l'instant lul).
		if (message.content.toLowerCase().includes('gdoc') || message.content.toLowerCase().includes('googledoc')) {
			replier.gDocs(message);
		}
		return;
	}

	//	sépare la commande et les args
	const args = message.content.slice(prefix.length).split(/ +/);
	const commandName = args.shift().toLowerCase();


	//	check si la commande, ou ses alias existent
	const command = client.commands.get(commandName) || client.commands.find(cmd => cmd.alias && cmd.alias.includes(commandName));
	if(!command) {
		message.channel.send(`C'est toi le ${commandName} !!!`);
		return ;
	}

	/*	On check si la commande est une commande temporaire, si elle ne l'est pas,
			on check si la commande est activé puis si elle a été envoyé en public ou en privé :
				- Si public on check si celui qui l'a envoyé est un admin ou un membre (non owner dans les deux cas)
						et on envoie en fonction
				- Si privé, on check simplement si il est un utilisateur simple
	*/
	if (!command.tmp &&
			(!command.status
			|| (!rCheck.isOwner(message.author) && command.status
				&& ((message.channel.type !== 'dm' && !rCheck.isAdmin(message.member) && (command.adminOnly || command.ownerOnly))
				|| (message.channel.type !== 'dm' && rCheck.isAdmin(message.member) && (command.ownerOnly))
				|| (message.channel.type === 'dm' && (command.adminOnly || command.ownerOnly)))))) return;

	//	check si c'est un message serverOnly
	if (command.serverOnly && message.channel.type !== 'text') {
		return message.reply('Je peux pas executer cette commande en privée');
	}

	//	check si c'est un message ayant besoin d'arguments
	if (command.args && !args.length) {
		let reply = 'Cette commande requiert des arguments.';
		if (command.usage) {
			reply += `\nL'utilisation correcte est : \`${prefix}${commandName} ${command.usage}\``;
		}
		return message.channel.send(reply);
	}


	//	tout ce qui est cooldown
	if (!cooldowns.has(command.name)) {
		cooldowns.set(command.name, new Discord.Collection());
	}

	const now = Date.now();
	const timestamps = cooldowns.get(command.name);
	const cooldownAmount = (command.cooldown || 3) * 1000;
	if (timestamps.has(message.author.id)) {
		const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

		if (now < expirationTime) {
			const timeLeft = (expirationTime - now) / 1000;
			return message.reply(`attendez ${timeLeft.toFixed(1)} seconde(s) avant de réutiliser \`${command.name}\`.`);
		}
	}

	timestamps.set(message.author.id, now);
	setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

	//	essaie d'executer la commande
	try {
		command.execute(message, args);
	}
	catch (error) {
		console.error(error);
		message.reply('y a un truc qui a merdé quelque part.... oopsies');
	}
});


client.login(token);
